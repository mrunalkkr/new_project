import React from 'react';
import Login from "./view/Authentication/login.js"
import { BrowserRouter, Navigate, Outlet, Route, Routes } from "react-router-dom";
import Home from './view/Home';
import Mlist from './view/Home/mlist';
import Slist from './view/Home/slist';
import Signup from './view/Authentication/signup'; 

function ProtectedRoute(){
  const isAuthenticated = localStorage.getItem("isAuthenticated");
  return isAuthenticated ? <Outlet/> : <Navigate to='/'/>
}
function App() {
    return (
     <BrowserRouter>
       <Routes>
       <Route exact path="/" element={<Signup  />} />
       <Route exact path="/Login" element={<Login  />} />
       <Route element={<ProtectedRoute/>}>
        <Route exact path="/dashboard" element={<Home />} />
       </Route>
       <Route exact path="/Mlist" element={<Mlist />}></Route>
       <Route exact path="/Slist" element={<Slist />}></Route>
       </Routes>
     </BrowserRouter>
    )
} 

export default App;